import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CourselistComponent } from './courselist/courselist.component';
import { DetailsComponent } from './details/details.component';

const appRoutes: Routes = [
  { path: "", component: CourselistComponent},
  { path: "home", component: CourselistComponent },
  { path: "details", component: DetailsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CourselistComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
