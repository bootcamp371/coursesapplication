export class Courses {
    constructor (public id:string, 
        public dept: string, 
        public courseNum: string , 
        public courseName: string, 
        public instructor: string,
        public startDate: string,
        public numDays: string){

    }
}
