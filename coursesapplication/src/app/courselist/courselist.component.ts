import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Courses } from '../models/courses.model';
import { CoursesService } from '../providers/courses.service';

@Component({
  selector: 'app-courselist',
  templateUrl: './courselist.component.html',
  styleUrls: ['./courselist.component.css']
})
export class CourselistComponent implements OnInit {

  wCourses: Array<Courses> = [];

  constructor(private router: Router, private getAllReviews: CoursesService) { }

  ngOnInit(): void {
    this.getAllReviews.getCourses().subscribe(data => {
      this.wCourses = data;
    });
  }

  viewDetails(idInput:string):void{
    this.router.navigate(['/details'],
    {
    queryParams: {
    id: idInput
    }
    }
    );
  }
}
