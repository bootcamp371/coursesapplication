import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Courses } from '../models/courses.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }

  private categoryEndpoint: string = 'http://localhost:8081/api/courses';
  private categoryByIdEndpoint: string = 'http://localhost:8081/api/courses/';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getCourses() : Observable<Courses[]> {
    return this.http.get(this.categoryEndpoint, this.httpOptions)
    .pipe(map(res => <Courses[]>res));
  }

  public getCourseById(id: number): Observable<Courses>{
    return this.http.get(this.categoryByIdEndpoint+id, this.httpOptions)
    .pipe(map(res => <Courses>res));
  }
}
