import { Component , OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Courses } from '../models/courses.model';
import { CoursesService } from '../providers/courses.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit{

  course!: any;
 // wCourses: Array<Courses> = [];

  constructor(
    private activatedRoute: ActivatedRoute,
        private myCoursesService: CoursesService
  ) { }

  ngOnInit() {
    //this.courses = this.myCoursesService.getCourses();
    // this.myCoursesService.getCourses().subscribe(data => {
    //   this.wCourses = data;
    // });
    
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];

      //this.song = this.getSong(id);
      this.myCoursesService.getCourseById(id).subscribe(data => {this.course = data});
      
    });
  }

  // getSong(id: number) {
  //   return this.songs.find((song) => song.id == id);
  // }
}
